import { loadRemoteModule } from '@angular-architects/module-federation';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';

const URL = 'http://localhost:3000/remoteEntry.js';

export const APP_ROUTES: Routes = [
    {
      path: '',
      component: AppComponent,
      pathMatch: 'full'
    },


    {
      path: 'posts',
      loadChildren: () => loadRemoteModule({
          type: 'module',
          remoteEntry: URL,
          exposedModule: './Module'
        })
        .then(m => m.PostsModule) 
    },

    {
      path: '**',
      component: NotFoundComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
